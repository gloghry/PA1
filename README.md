PA1 (also known as stick(s)) was given by N Macias in CLark College's 224 CSE class. It is a game of sticks, and is intended to be played by one player vs the computer. The game was started Sept 30th 2019, and submitted for grading Oct. 4th 2019.

HOW-TO --
The user is promted to select how many sticks they would like to play with. While there is no upper limit, there must be at least 10 sticks on the feild to start with.

Next the game will ask if the (u)ser would like to go first, or if they would like the (c)omputer to go first. In future versions, I may look into a (r)andom setting, but that has not been implemented as of submission.

Now the game of sticks starts. The user and the computer take turns grabbing sticks. They may not grab more than 3 and are not permitted to not grab any sticks ie grab (0) sticks. They may not grab more sticks than are on the feild. If either of these rules are broken twice, the game quits.

Once the sticks are down to 0 sticks left on the feild, the person who grabed the last set of sticks wins. For example, if there were four (4) sticks and the comuter grabed one (1), I would grab the remaining three (3) sticks to bring down the total count to 0. Thus I win. The same logic applies to the computer as well.

I hope you enjoy my game of sticks! --

-Garett Loghry
